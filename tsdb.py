import os
basedir = os.path.dirname(os.path.abspath(__file__))

import json
import click
import socket
import statsd
import requests
from time import sleep
from delorean import parse as dparse
from fake_useragent import UserAgent as FakeUserAgent
from httpagentparser import detect as uaparse

from ConfigParser import ConfigParser

class UserActionFetcher(object):

    def __init__(self, endpoint, batch_size):
        self.endpoint = endpoint
        self.batch_size = batch_size
        self.offset = 0
    
    def next(self):
        """
        fetch <self.batch_size> count of events at a time
        """
        req_params = { 
            'limit': self.batch_size,
            'offset': self.offset,
            }
        resp = requests.get(self.endpoint, params=req_params)
        data = resp.json()
        total = int(data['actions']['total'])
        if self.offset >= total:
            # no more events to fetch
            raise EOFError
        actions = resp.json()['actions']['action']
        self.offset += len(actions)
        return actions
    
class Loader(object):
    
    def __init__(self, endpoint, batch_size, count):
        """
        The idea here is that we can use this loader script to populate the 
        date in a set of backends configured in the cfg file
        """
        self.fake_ua = FakeUserAgent()
        self.fetcher = UserActionFetcher(endpoint, batch_size)
        self.count = count

    def load(self):
        while True:
            try:
                actions = self.fetcher.next()
                self.store(actions)
                click.echo("fetched/loaded %d items so far" % self.fetcher.offset)
                sleep(1)
                if self.count is not None and self.fetcher.offset >= self.count:
                    break
            except EOFError:
                break
        click.echo("done.")
        
    def fixup_action(self, a):
        ua = uaparse(self.fake_ua.random)
        a['browser'] = ua['browser']['name']
        a['browser_version'] = ua['browser']['version']
        a['os'] = ua['platform']['name']
    
    def fixup_actions(self, actions):
        for a in actions:
            self.fixup_action(a)
        
    def store(self, actions):
        self.fixup_actions(actions)
        self.store_actions(actions)
        
    def store_actions(self, actions):
        raise NotImplementedError()
        
class InfluxLoader(Loader):
    
    def __init__(self, *args, **kwargs):
        super(InfluxLoader, self).__init__(*args)

        database = kwargs.get('database')
        host = kwargs.get('host')
        port = kwargs.get('port')

        self.auth = {
            'u': kwargs.get('username'),
            'p': kwargs.get('password')
        }

        # create the database
        influx_base_url = "http://%s:%s/db" % (host, port)
        data = {'name': database}
        r = requests.post(influx_base_url, params=self.auth, data=json.dumps(data))
        if r.status_code not in [201, 409]: # created or exists
            raise RuntimeError(r.text)

        self.influx_series_url = "%s/%s/series" % (influx_base_url, database)
        self.sequence_number = 1
            
    def fixup_action(self, a):
        super(InfluxLoader, self).fixup_action(a)
        # set our own influxdb event time based on the create date
        a['time'] = int(dparse(a['created']).epoch())
        a['sequence_number'] = self.sequence_number
        self.sequence_number += 1
        
    def store_actions(self, actions):

        columns = ['mediapackageId','userId','userIp','length', 'type',
                   'browser','browser_version','os','time','created','sequence_number']
        data = [{
                 'time_precision': 's',
                 'name': 'user_actions',
                 'columns': columns,
                 'points': [[a[col] for col in columns] for a in actions]
        }]
        data = json.dumps(data)
        r = requests.post(self.influx_series_url, params=self.auth, data=data)
        return
    
class LogstashLoader(Loader):
    
    def __init__(self, *args, **kwargs):
        super(LogstashLoader, self).__init__(*args)
        host = kwargs.get('host')
        port = kwargs.get('port')
        self.logstash_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.logstash_socket.connect((host, int(port)))
    
    def store_actions(self, actions):
        for a in actions:
            event = json.dumps(a)
            self.logstash_socket.sendall(event + "\n")
    
class GraphiteLoader(Loader):
    
    def __init__(self, *args, **kwargs):
        super(GraphiteLoader, self).__init__(*args)
        host = kwargs.get('statsd_host')
        port = kwargs.get('statsd_port')
        prefix = kwargs.get('metric_prefix')
        self.statsd = statsd.StatsClient(prefix='matterhorn')
        self.time_compression = int(kwargs.get('time_compression'))
        self.prev_epoch = None
        
    def store(self, actions):
        self.store_actions(actions)

    def store_actions(self, actions):
        for a in actions:
            created_epoch = dparse(a['created']).epoch()
            if self.prev_epoch is not None:
                """
                take the time difference between the previously seen action and the current
                one and then calculate 1/x of that where x is the configured time_compression.
                then sleep for that amount of time to simulate an accelerated version
                of the action events timeline
                """
                epoch_diff = created_epoch - self.prev_epoch
                if epoch_diff > 0:
                    delay = epoch_diff / self.time_compression
                    sleep(delay)
            self.prev_epoch = created_epoch
            self.statsd.set('users', a['userId'])
            self.statsd.set('ips', a['userIp'])
            self.statsd.set('mediapackage', a['mediapackageId'])
            self.statsd.incr('event')
            if a['type'] == 'NORMAL_STARTUP':
                self.statsd.incr('normal_startup')
            if a['type'].startswith('RESIZE'):
                self.statsd.incr('resize')

        
@click.group()
@click.option('--config', default='%s/tsdb.cfg' % basedir, type=click.File('r'), help='path to config file')
@click.option('--count', default=None, type=click.INT, help='number of actions to fetch. default is all')
@click.option('--batch_size', default=1000, help='number of actions to fetch/store per request')
@click.pass_context
def cli(ctx, config, count, batch_size):
    ctx.obj = {}
    ctx.obj['CONFIG'] = ConfigParser()
    ctx.obj['CONFIG'].readfp(config)
    ctx.obj['COUNT'] = count
    if count is not None:
        ctx.obj['BATCH_SIZE'] = count
    else:
        ctx.obj['BATCH_SIZE'] = batch_size
    ctx.obj['ENDPOINT'] = ctx.obj['CONFIG'].get('matterhorn','endpoint')
        
@cli.command()
@click.pass_context
def influxdb(ctx):

    config = dict(ctx.obj['CONFIG'].items('influxdb'))
    influx = InfluxLoader(ctx.obj['ENDPOINT'], ctx.obj['BATCH_SIZE'], ctx.obj['COUNT'], **config)
    influx.load()
    
@cli.command()
@click.pass_context
def logstash(ctx):
    
    config = dict(ctx.obj['CONFIG'].items('logstash'))
    lstash = LogstashLoader(ctx.obj['ENDPOINT'], ctx.obj['BATCH_SIZE'], ctx.obj['COUNT'], **config)
    lstash.load()
    
@cli.command()
@click.pass_context
def graphite(ctx):
    
#     import pydevd
#     pydevd.settrace()

    config = dict(ctx.obj['CONFIG'].items('graphite'))
    grph = GraphiteLoader(ctx.obj['ENDPOINT'], ctx.obj['BATCH_SIZE'], ctx.obj['COUNT'], **config)
    grph.load()
    
        
        