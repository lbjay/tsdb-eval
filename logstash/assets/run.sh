#!/bin/bash

set -e
set -x

LOGSTASH_CONFIG_FILE="/opt/logstash.conf"

# if you provide a value for $LOGSTASH_CONFIG_URL the contents will override the
# provided config
if [ -n "${LOGSTASH_CONFIG_URL}" ]; then
	wget $LOGSTASH_CONFIG_URL -O $LOGSTASH_CONFIG_FILE
fi

# This magic will replace ES_HOST and ES_PORT in your logstash.conf
# file if they exist with the IP and port dynamically generated
# by docker. Take a look at the readme for more details.
sed -e "s/ES_HOST/${ES_PORT_9200_TCP_ADDR}/g" \
    -e "s/ES_PORT/${ES_PORT_9200_TCP_PORT}/g" \
    -i $LOGSTASH_CONFIG_FILE

# Fire up logstash!
exec /opt/logstash/bin/logstash \
     agent \
     --config $LOGSTASH_CONFIG_FILE \
     -- \
     web