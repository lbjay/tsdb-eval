from setuptools import setup

setup(
    name='tsdb',
    version='0.1',
    py_modules=['tsdb'],
    install_requires=[
        'click',
        'requests==2.2.1',
        'delorean',
        'httpagentparser',
        'fake_useragent',
        'fig',
        'statsd'
    ],
    entry_points='''
        [console_scripts]
        tsdb=tsdb:cli
    ''',
)
