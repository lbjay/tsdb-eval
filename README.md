# README #

Scripts and containers for evaluating a few *time series database* solutions for
storing, querying and visualizing some Matterhorn events and metrics.

### Requirements ###

* docker
* python 2.7+

### Setup ###

1. clone the repo: `git clone git@bitbucket.org:lbjay/tsdb-eval.git; cd tsdb-eval`
1. Install docker: https://docs.docker.com/installation/
1. Create and activate a python virtualenv (optional but recommended)

        virtualenv venv
        source venv/bin/activate

1. run `pip install --editable .` to install the python deps and the tsdb script
1. take a look at `tsdb.cfg` and `fig.yml`. You shouldn't have to make any changes unless you need/want to use alternate ports or user:pass combos.
1. in a second terminal execute `fig up` to bring up the docker containers. There are two: the influxdb instance and a grafana front-end. This part will take awhile the first time you run it as docker needs to pull down the container images from the docker.io registry.
1. execute `tsdb fetch --batch_size=1000`. This will fetch the entries in the `mh_user_action` table and load them into the configured "backends" (currently just influxdb).

### UIs ###

There are a couple of ways to view the loaded data at this point:

* through the influxdb admin: http://localhost:8083/, login is *root*:*foobar*
* through the grafana dashboard: http://localhost:8080, http auth is *admin*:*foobar*

#### Influxdb Admin ####

Connect to the instance through the admin UI using the *root*:*foobar* user:pass combo. The `matterhorn` database should be listed there. Click on *Explore Data* to get to the query interface. Try some queries using http://influxdb.com/docs/v0.8/api/query_language.html as a guide.

A questionably useful example query that at least gets you a graph is something like,

        select count(type), browser from user_actions group by time(10s), type

#### Grafana Dashboard ####

I haven't managed to get the right query to display any data yet. :(
